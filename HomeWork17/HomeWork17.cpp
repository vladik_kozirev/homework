﻿// HomeWork17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <math.h>

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector() :x(2), y(1), z(1)
    {}
    Vector(int _x,float _y,double _z) :x(_x), y(_y), z(_z)
    {}
    void xyz() 
    {
        std::cout << x << " " << y << " " << z << "\n";
    }
    void Show() 
    { 
        std::cout << x * x + y * y << "\n"; 
    }
    void Gipot() 
    { 
        std::cout << hypot(x, y); 
    }
};
int main()
{
    Vector X(3,4.4,5.1);
    Vector Y(6, 3.3, 1.2);
    Vector Z(2, 7.6, 8.1);
    X.xyz();
    Y.xyz();
    Z.xyz();
    X.Show();
    X.Gipot();
}
 

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
